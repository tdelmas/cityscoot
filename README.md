Plainte au Jury de Déontologie Publicitaire suite à la vidéo posté sur Twitter et Facebook par Cityscoot :

Tweet d'origine (10 oct) : 

- https://twitter.com/CityscootParis/status/1050033333877596160
- Archive du 1 dec : https://web.archive.org/web/20181201125012/https:/twitter.com/CityscootParis/status/1050033333877596160
- Copie d'écran : https://framagit.org/tdelmas/cityscoot/raw/master/tweet.png
- Vidéo : https://framagit.org/tdelmas/cityscoot/raw/master/video-cityscoot-tweet.mp4

Tweets alertant cityscoot du problème (17 oct) : https://twitter.com/RasLeScoot/status/1052499374641635328

Plainte (18 nov) : https://framagit.org/tdelmas/cityscoot/raw/master/Plainte_Cityscoot.pdf

Complément (26 nov) : https://framagit.org/tdelmas/cityscoot/raw/master/Plainte_Cityscoot_arguments_compl%C3%A9mentaires.pdf

Réponse de Cityscoot (26 nov) : https://framagit.org/tdelmas/cityscoot/raw/master/reponse_cityscoot.pdf

Scéance du Jury de Déontologie Publicitaire : 7 décembre

> Brouillon des arguments présentés devant le jury : https://framagit.org/tdelmas/cityscoot/raw/master/brouillon_r%C3%A9ponse_aux_arguments_de_cityscoot.pdf

> Après la scéance, les deux représentants de Cityscoots ont acceptés de rester discuter avec moi de façon informelle.


Message pour Cityscoot après l'audience (18 dec) : https://framagit.org/tdelmas/cityscoot/raw/master/Apr%C3%A8s%20audience%20JDP.txt

Suppréssion du Tweet d'origine https://twitter.com/CityscootParis/status/1050033333877596160 : 18 dec, juste après envois du message précédant.

Avis préliminaire du JDP (12 dec): https://framagit.org/tdelmas/cityscoot/raw/master/AVIS_CITYSCOOT.pdf

Ma réponse à l'Avis préliminaire du JDP (12 dec): https://framagit.org/tdelmas/cityscoot/raw/master/R%C3%A9ponse_%C3%A0_l_Avis.pdf

Avis definitif du JDP (8 janv. 2019): "Plainte fondée" https://www.jdp-pub.org/avis/cityscoot-internet/ (https://framagit.org/tdelmas/cityscoot/raw/master/AVIS_CITYSCOOT_FINAL.TXT)
